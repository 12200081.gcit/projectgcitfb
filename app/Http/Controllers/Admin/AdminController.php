<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Log;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;
use Psy\TabCompletion\AutoCompleter;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }



    public function facilities()
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        return view('admin.facilities', compact('categories', 'subcategories'));
    }
    public function createfacility(Request $request)
    {
        // dd($request->all());
        // DB::enableQueryLog();
        // dd(DB::getQueryLog());
        try {
            $request->validate([
                'category_name' => ['required', 'string', 'max:255', 'unique:categories'], 
                'description' => ['string', 'max:255'],
                'image' => [ 'image', 'mimes:jpeg,png,jpg', 'max:2048'],
            ]);
    
            $category = Category::where('category_name', $request->category_name)->first();
        
            if ($category) {
                return redirect()->route('admin.facilities')->with('error', 'This category is already created!');
            }
    
            $imagePath = $request->file('image')->store('public/images'); 
            Category::create([
                'category_name' => $request->category_name,
                'description' => $request->description,
                'image' => $imagePath, // Store the image path in the database
            ]);
            return redirect()->route('admin.facilities')->with('success', 'Category has been created');
        }catch (\Exception $e) {
            dd($e->getMessage());
            Log::error('Error creating category: ' . $e->getMessage());
            return redirect()->route('admin.facilities');
        }
    }
    
   
    public function createsubfacility(Request $request)
{
    
   
    // Validate the form data, including the image upload
    $request->validate([
        'facility_name' => ['required', 'string', 'max:255'],
        'category_id' => ['required', 'integer', 'exists:categories,id'],
        'image' => ['required', 'image', 'mimes:jpeg,png,jpg', 'max:2048'], // Adjust validation rules for image files
    ]);

    // Handle the image upload and save it to a storage location
    $imagePath = $request->file('image')->store('images', 'public'); // Adjust the storage location as needed

    // Check if the subcategory already exists
    $subcategory = Subcategory::where('facility_name', $request->facility_name)->first();

    if ($subcategory) {
        // Subcategory already exists, show an error message
        return redirect()->route('admin.facilities')->with('error', 'This facility is already created!');
    }

    // If the subcategory does not exist, create it
    Subcategory::create([
        'facility_name' => $request->facility_name,
        'category_id' => $request->category_id,
        'resource' => $request->resource,
        'time' => $request->time,
        'image' => $imagePath, // Store the image path in the database
    ]);

    return redirect()->route('admin.facilities')->with('success', 'Facility has been created');
}




    public function user()
    {
        $users = User::all();
        return view('admin.user', compact('users'));
    }
    public function createuser(Request $request)
    {
        // dd($name, $email, $usertype, $role); 
   
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            // 'usertype' => ['required', 'string', 'max:255'],
            // 'role' => ['required', 'integer', 'max:255'],
            // 'password' => ['required', Rules\Password::defaults()],
        ]);
        $user = User::where('email', $request->email)->first();

        if ($user) {
            // Subcategory already exists, show an error message
            return redirect()->route('admin.user')->with('error', 'This user already exist!');
        }
    
        // Create the user
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'usertype' => $request->usertype,
            'role' => $request->role,
        ]);
            // User creation failed, handle the error and return an error message
        return redirect()->route('admin.user')->with('success', 'User is successfully added.');
        
    }
    public function deleteUsers($id) {
      
        $user = User::findOrFail($id);
        $user->delete();
            // Redirect back with a success message
        return redirect()->route('admin.user')->with('success', 'User is successfully deleted.');
      
    }


    public function booking()
    {
        return view('admin.booking');
    }
    public function setting()
    {
        return view('admin.setting');
    }
}
