<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function detail(Subcategory $subcategory)
    {
        return view('detail', compact('subcategory'));
    }
    public function mybooking()
    {
        return view('mybooking');
    }
    public function booknow()
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        return view('booknow', compact('subcategories'), compact('categories'));
    }
}
