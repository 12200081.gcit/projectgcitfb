<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = ['facility_name', 'category_id', 'image', 'resource', 'start_time', 'end_time', 'method'];
    
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function bookings()
    {
        return $this->hasMany(Booking::class, 'subcategory_id');
    }
}
