<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
                'name'     => 'admin',
                'email'    => '12200081.gcit@rub.edu.bt',
                'role'     => 'admin',
                'usertype' => 'admin',
                'password' => bcrypt('admin')  //Hash::make('john@123')
        ]);
        DB::table('users')->insert([
            'name'     => 'Sonam',
            'email'    => '12200064.gcit@rub.edu.bt',
            'role'     => 'user',
            'usertype' => 'usr',
            'password' => bcrypt('user')  //Hash::make('john@123')
    ]);
    }
}
