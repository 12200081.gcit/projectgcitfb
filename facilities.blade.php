<x-admin-component>
        <!-- main sidebar -->
        <div class="main-content">
            <div class="row g-10">
                <div class="col-xl-12 col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModalCenter" style="background-color: #3498db">Add Category</button>
                            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModalCenterFacility" style="background-color: #3498db">Add Facility</button>
                            <button type="button" class="btn btn-danger" onclick="fireSweetAlert()" style="background-color: red">Delete Category</button>
                        </div>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterLabel">Add Category</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route('admin.createfacility') }}" method="POST" enctype="multipart/form-data">
                                                @csrf <!-- Add CSRF token for security -->
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="category_name">Category Name</label>
                                                    <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Category Name" required/>
                                                </div>
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="description">Description</label>
                                                    <input type="text" class="form-control" id="description" name="description" placeholder="Description" required/>
                                                </div>
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="image">Category Image</label>
                                                    <input type="file" class="form-control" id="image" name="image" required/>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-success" style="background-color: #3498db; color:white">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                

                <div class="panel mb-10 mt-10">
                        <div class="modal fade" id="exampleModalCenterFacility" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterLabel">Add Facility</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route('admin.createsubfacility') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                            
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="exampleInputDetails">Facility Name</label>
                                                    <input type="text" class="form-control" id="exampleInputDetails" name="facility_name" placeholder="Facility Name">
                                                </div>
                                            
                                                <div class="form-group mb-20">
                                                    <label class="form-label">Category</label>
                                                    <select name="category_id" class="form-control">
                                                        <option>--select--</option>
                                                        @foreach($categories as $category)
                                                            <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    
                                                </div>
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="exampleInputDetails">Resources</label>
                                                    <input type="text" class="form-control" id="exampleInputDetails" name="resource" placeholder="Resources">
                                                </div>
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="exampleInputDetails">Available Time</label>
                                                    <input type="text" class="form-control" id="exampleInputDetails" name="time" placeholder="Available Time">
                                                </div>
                                            
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="exampleInputFile">Facility Image</label>
                                                    <input type="file" class="form-control" id="exampleInputFile" name="image">
                                                </div>
                                            
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-success" style="background-color: #3498db; color: white">Save</button>
                                                </div>
                                            </form>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                    
                            <div class="col-xl-12 col-lg-12">
                                <div class="panel">
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover" id="sportsTable">
                                            <thead>
                                              <tr>
                                                <th>Sl.No</th>
                                                <th>Category Image</th>
                                                <th>Category Name</th>
                                                <th>Description</th>
                                                <th>Actions</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $serialNumber = 1;
                                                @endphp
                                                @foreach($categories as $category)
                                              <tr>
                                                <td>{{ $serialNumber }}</td>
                                                <td><img src="{{ asset('public' . $category->image) }}" alt="Facility Image" style="border-radius: 0"></td>
                                                <td>{{ $category->category_name }}</td>
                                                <td>{{ $category->description }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModalCenterEditFacility" style="background-color: #3498db">Edit</button>
                                                    <div class="modal fade" id="exampleModalCenterEditFacility" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
            
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterLabel">Edit Facility</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {{-- <form>              
                                                                    <div class="form-group mb-20">
                                                                        <label class="form-label" for="exampleInputFacilityname">Name</label>
                                                                        <input type="text" class="form-control" id="exampleInputFacilityname" placeholder="Name">
                                                                    </div>
                                                                    
                                                                    <div class="form-group mb-20">
                                                                        <label class="form-label" for="exampleInputDetails">Facility</label>
                                                                        <input type="text" class="form-control" id="exampleInputDetails" placeholder="Facility">
                                                                    </div>
                                                                </form> --}}
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-success" onclick="fireSweetAlertEditSaved()">Save</button>                                        
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                    <button type="button" class="btn btn-danger" onclick="fireSweetAlertFacility()" style="background-color: red" >Delete</button>                                   
                                                </td>
                                              </tr>
                                                    @php
                                                        $serialNumber++;
                                                    @endphp
                                              @endforeach
                                            </tbody>
                                            </table>  
                                </div>    
                                </div>
                                </div>

                    <div class="col-xl-12 col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-striped table-hover" id="sportsTable">
                                <thead>
                                  <tr>
                                    <th>Sl.No</th>
                                    <th>Facility Image</th>
                                    <th>Facility Name</th>
                                    <th>Category</th>
                                    <th>Resources</th>
                                    <th>Available Time</th>
                                    <th>Booking Method</th>
                                    <th>Actions</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $serialNumber = 1;
                                    @endphp
                                    @foreach($subcategories as $subcategory)
                                  <tr>
                                    <td>{{ $serialNumber }}</td>
                                    <td>
                                        <img src="{{ asset('storage/' . $subcategory->image) }}" alt="Facility Image" style="border-radius: 0">
                                    </td>
                                    <td>{{ $subcategory->facility_name }}</td>
                                    <td>{{ $subcategory->category->category_name }}</td>
                                    <td>{{  $subcategory->resource }}</td>
                                    <td>{{  $subcategory->time }}</td>
                                    <td></td>
                                    
                                    
                                    <td>
                                        <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModalCenterEditFacility" style="background-color: #3498db">Edit</button>
                                        <div class="modal fade" id="exampleModalCenterEditFacility" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">

                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalCenterLabel">Edit Facility</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <form>              
                                                        <div class="form-group mb-20">
                                                            <label class="form-label" for="exampleInputFacilityname">Name</label>
                                                            <input type="text" class="form-control" id="exampleInputFacilityname" placeholder="Name">
                                                        </div>
                                                        
                                                        <div class="form-group mb-20">
                                                            <label class="form-label" for="exampleInputDetails">Facility</label>
                                                            <input type="text" class="form-control" id="exampleInputDetails" placeholder="Facility">
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" onclick="fireSweetAlertEditSaved()">Save</button>                                        
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <button type="button" class="btn btn-danger" onclick="fireSweetAlertFacility()" style="background-color: red" >Delete</button>                                   
                                    </td>
                                  </tr>
                                  @php
                                  $serialNumber++;
                                @endphp
                                  @endforeach
                                </tbody>
                                </table>      
                        </div>
                    </div>
                </div>
                   
                </div>

            </div>
            
            
        </div>
    </div>

    <script>
  
        // Initally only show sports table
        $('.table').addClass('d-none'); 
        $('#sportsTable').removeClass('d-none');

        $('.nav-link').click(function() {

        // Toggle active class
        $('.nav-link').removeClass('active');
        $(this).addClass('active');

        // Show/hide tables
        var target = $(this).data('target');
        $('.table').addClass('d-none');  
        $(target).removeClass('d-none');

        });

    </script>
{{-- ---------------------------------------------- --}}
{{-- for adding category --}}
<script>
   $(document).ready(function() {
    $('#category-form').on('submit', function(e) {
        e.preventDefault(); // Prevent the default form submission

        // Submit the form using the default browser behavior
        // This will send the data to the server and trigger a page reload or navigation
        // If the form submission is successful, the success message will be displayed

        // No need for AJAX or FormData here

        // You can display the success message using SweetAlert or any other method
        Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Category has been saved',
            showConfirmButton: false,
            timer: 1500
        });
    });
});
</script>

    <script>

    function fireSweetAlert() {
        Swal.fire({
            title: 'Are you sure?',
            
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#5cb85c',
            cancelButtonColor: '#d9534f',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                'Deleted!',
                'Category has been deleted.',
                'success'
                )
            }
            })
    }
    </script>
    <script>
        function fireSweetAlertFacility() {
            Swal.fire({
                title: 'Are you sure?',
                
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#5cb85c',
                cancelButtonColor: '#d9534f',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                    'Deleted!',
                    'Facility has been deleted.',
                    'success'
                    )
                }
                })
        }
    </script>
    
    <script>
        function fireSweetAlertFacilitySaved() {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Facility has been saved',
                showConfirmButton: false,
                timer: 1500
                })
        }
    </script>

    <script>
        function fireSweetAlertEditSaved() {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Update has been saved',
                showConfirmButton: false,
                timer: 1500
                })
        }
    </script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if (session('success'))
    <script>
        Swal.fire({
            position: 'top',
            icon: 'success',
            title: '{{ session('success') }}',
            showConfirmButton: false,
            timer: 1500
        });
    </script>
@endif

@if (session('error'))
    <script>
        Swal.fire({
            position: 'top',
            icon: 'error',
            title: '{{ session('error') }}',
            showConfirmButton: false,
            timer: 1500
        });
    </script>
@endif
</x-admin-component>