<x-admin-component>
        <div class="main-content">
            <div class="row g-10">
            <div class="col-xl-12 col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModalCenter" style="background-color: #3498db">Add User</button>
                            <button type="button" class="btn btn-danger" onclick="fireSweetAlertDeleteUser()" style="background-color: red">Delete User</button>
                        </div>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" >
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterLabel">Add User</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                                        </div>
                                        <div class="modal-body">
                                                <form action="{{ route('admin.createuser') }}" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    <!-- Name -->
                                                    <div class="form-group mb-20">
                                                        <label class="form-label" for="name">Name</label>
                                                        <input class="form-control" type="text" name="name" required autocomplete="name" />   
                                                    </div>
                                                    <!-- Email Address -->
                                                    <div class="form-group mb-20">
                                                        <label class="form-label" for="email">Email</label>
                                                        <input class="form-control" type="email" name="email" required autocomplete="username" />    
                                                    </div>
                                                    <!-- User Type -->
                                                    <div class="form-group mb-20">
                                                        <label class="form-label" for="usertype">User Type</label>    
                                                        <input class="form-control" type="text" name="usertype" required autocomplete="usertype" /> 
                                                    </div>
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="role">User Role:</label>&nbsp;
                                                    <input  class="form-control" type="radio" name="role" value="user" {{ old('role') == 'user' ? 'checked' : '' }} required autocomplete="role">&nbsp;User&nbsp;
                                                    <input class="form-control" type="radio" name="role" value="admin" {{ old('role') == 'admin' ? 'checked' : '' }} required autocomplete="role">&nbsp;Admin
                                                </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success" style="background-color: #3498db; color:white">
                                                            Add
                                                        </button>
                                                    </div>
                                                </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="panel mb-10 mt-10">
                   
                    <div class="col-xl-12 col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Sl_No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                        <th>Role</th>
                                        <td><button type="button" class="btn btn-danger" style="background-color: red">Delete All</button></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $serialNumber = 1;
                                    @endphp

                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{ $serialNumber }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->usertype }}</td>
                                        <td>{{ $user->role }}</td> 
                                        <td>
                                            <button type="submit" class="btn btn-danger" style="background-color: red"  onclick="fireSweetAlertDeleteUser('{{ $user->id }}')">Delete User</button>
                                        </td>   
                                    </tr>
                                        @php
                                            $serialNumber++;
                                        @endphp
                                    @endforeach           
                                    </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>       
        </div>
        <script>
            function fireSweetAlertDeleteUser(userId) {
                Swal.fire({
                    title: 'Are you sure you want to delete this user?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#5cb85c',
                    cancelButtonColor: '#d9534f',
                    confirmButtonText: 'Yes',
                }).then((result) => {
                    if (result.isConfirmed) {
                        // Make an Axios DELETE request to delete the user
                        axios.delete(`user/delete/${userId}`)     
                    }
                });
            }
        </script>
        

 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if (session('success'))
    <script>
        Swal.fire({
            position: 'top',
            icon: 'success',
            title: '{{ session('success') }}',
            showConfirmButton: false,
            timer: 1500
        });
    </script>
@endif

@if (session('error'))
    <script>
        Swal.fire({
            position: 'top',
            icon: 'error',
            title: '{{ session('error') }}',
            showConfirmButton: false,
            timer: 1500
        });
    </script>
@endif
</x-admin-component>