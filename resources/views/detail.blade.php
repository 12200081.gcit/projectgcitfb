
<x-app-layout><br/><br/>
    <main id="main">


        <!-- ======= Portfolio Details Section ======= -->
        <section id="portfolio-details" class="portfolio-details">
          <div class="container">
    
            <div class="row gy-4">
    
              <div class="col-lg-8">
                <div class="portfolio-details-slider swiper ">
                  <div class="swiper-wrapper align-items-center">
                    <div class="swiper-slide" >
                      <img src="{{ asset('storage/' . $subcategory->image) }}" class="img-fluid" alt="">
                    </div>
                  </div>
                  {{-- <div class="portfolio-description">
                    <h2>About Conference Facility</h2>
                    <p>
                      Description
                    </p><br/><br/><br/>
                  </div> --}}
                </div>
              </div>
    
              <div class="col-lg-4">
                <div class="portfolio-info">
                  <h3>Facility information</h3>
                  <ul>
                    <li><strong>Name</strong>: {{ $subcategory->facility_name }}</li>
                    <li><strong>Category</strong>: {{ $subcategory->category->category_name }}</li>
                    <li><strong>Resources</strong>: {{ $subcategory->resource }}</li>
                    <li><strong>Available from</strong>: {{ $subcategory->time }}</li>
                  </ul>
                </div>
                <div class="card" style="margin-top: 5px">
                  <div class="card-body">
                    <form class="row g-3" id="myForm">
                      <!-- Your form fields here -->
                      <div class="col-md-6">
                        <label for="inputCity" class="form-label">Date</label>
                        <input type="date" class="form-control" id="inputCity" required min="<?php echo date('Y-m-d'); ?>">
                      </div>
                      <div class="col-md-4">
                        <label for="inputState" class="form-label">Time</label>
                        <select id="dropdown" required>
                          <option value="">__Select__</option>
                          <option value="">09am - 10am</option>
                          <option value="">10am - 11am</option>
                          <option value="">11am - 12pm</option>
                          <option value="">12pm - 1pm</option>
                          <option value="">01pm - 02pm</option>
                          <option value="">02pm - 03pm</option>
                          <option value="">03pm - 04pm</option>
                          <option value="">04pm - 05pm</option>
                      </select>
                      </div>
                      <div class="col-12">
                        <label>Additional Requirement</label>
                        <textarea class="form-control" name="message" rows="6" required></textarea>
                      </div>
                      <div class="text-center">
                          <button type="button" id="showConfirmationModal" class="btn btn-primary" style="color: black">Submit</button>
                      </div>
                  </form>
                  
                  <!-- Bootstrap Modal for Confirmation -->
                  <div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="confirmationModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="confirmationModalLabel">Confirmation</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  Are you sure you want to book this facility?
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color: #3498db; color:white">Cancel</button>
                                  <button type="button" id="confirmSubmit" class="btn btn-primary" style="background-color: #3498db; color:white">Confirm</button>
                              </div>
                          </div>
                      </div>
                  </div>
                  
                   

      
                  </div>
                </div>
                




                
              </div>
    
            </div>
    
          </div>
        </section><!-- End Portfolio Details Section -->
        
   
    
      </main><!-- End #main --><br/>
      
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>
<script>
  document.addEventListener('DOMContentLoaded', function () {
      // Initialize Bootstrap
      var myModal = new bootstrap.Modal(document.getElementById('confirmationModal'));
      const form = document.getElementById('myForm');
      const showConfirmationModalBtn = document.getElementById('showConfirmationModal');
      const confirmSubmitBtn = document.getElementById('confirmSubmit');

      showConfirmationModalBtn.addEventListener('click', function () {
          myModal.show();
      });

      confirmSubmitBtn.addEventListener('click', function () {
          form.submit(); // Submit the form when the user confirms
      });
  });
</script>



    
      
    </x-app-layout>

 