<!-- ======= Header ======= -->
<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

        <div class="logo">
        <h1><a href="{{ url('dashboard') }}"><img src="{{ asset('welcome_assets/img/logo.png') }}" alt="logo"/></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav id="navbar" class="navbar">
        <ul>
            <li><a class="nav-link scrollto active" href="/dashboard#hero">Home</a></li>
            <li><a class="nav-link scrollto" href="/dashboard#about">About</a></li>
            {{-- <li><a class="nav-link scrollto" href="#services">Services</a></li> --}}
            <li><a class="nav-link scrollto " href="/dashboard#service">Facility</a></li>
            {{-- <li><a class="nav-link scrollto" href="#testimonials">Team</a></li> --}}
            <li><a class="nav-link scrollto" href="/dashboard#contact">Contact Us</a></li>
            <li>
                @if (Route::has('login'))
                    @auth
                        <a href="{{ url('/booknow') }}" class="getstarted scrollto">Book Now</a>
                    @else
                        <a href="{{ route('login') }}" class="getstarted scrollto">Log in</a>
                    @endauth
                @endif  
            </li>
            <li class="dropdown"><a href="#"><span>{{ Auth::user()->name }}</span> <i class="bi bi-chevron-down"></i></a>
                <ul>
                  <li>
                    <x-dropdown-link href="{{ url('mybooking')}}">
                        {{ __('MyBooking') }}
                    </x-dropdown-link>
                  </li>
                  <li> 
                    <x-dropdown-link :href="route('profile.edit')">
                        {{ __('Profile') }}
                    </x-dropdown-link>
                  </li>
                  <li>
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf

                        <x-dropdown-link :href="route('logout')"
                                onclick="event.preventDefault();
                                            this.closest('form').submit();">
                            {{ __('Log Out') }}
                        </x-dropdown-link>
                    </form>
                  </li>
                 
                </ul>
              </li>
          
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
    </header><!-- End Header -->