<x-admin-component>
    
        <div class="main-content">
          

            <div class="row g-10">
                <div class="col-xl-12 col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <button type="button" class="btn btn-danger" onclick="fireSweetAlertDeleteBooking()" style="background-color: red;color:white">Delete Booking</button>      
                        </div>
                    </div>
                </div>
                <div class="panel mb-10 mt-10">
                    <div class="col-xl-12 col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-striped table-hover">
                                <thead>
                                  <tr>
                                    <th>
                                      <span class="custom-checkbox">
                                                        <input type="checkbox" id="selectAll">
                                                        <label for="selectAll"></label>
                                                    </span>
                                    </th>
                                    <th>Facility</th>
                                    <th>Category</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>User email</th>
                                    <th>Additional Requirement</th>
                                    <td>Reason</td>
                                    <th>Status</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <span class="custom-checkbox">
                                              <input type="checkbox" id="checkbox1" name="options[]" value="1">
                                              <label for="checkbox1"></label>
                                                    </span>
                                    </td>
                                    <td>Football</td>
                                    <td>3-11-2023<br>5:00pm</td>
                                    <td>Domtu</td>
                                    <td>Ball,flags</td>
                                    <td>Training dress</td>
                                    <td>Pending</td>
                                    

                                    
                                    
                                    <td>
                                    <button type="button" class="btn btn-success btn-sm" onclick="fireSweetAlertApprove()"><i class="bi bi-check-circle"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" onclick="fireSweetAlertReject()"><i class="bi bi-x-circle"></i></button>
                                    <!-- <a href="#editEmployeeModal" class="approve" data-toggle="modal">
                                            <i class="bi bi-check-circle" data-toggle="tooltip" title="approve"></i></a>
                                      <a href="#deleteEmployeeModal" class="reject" data-toggle="modal">
                                            <i class="bi bi-x-circle" data-toggle="tooltip" title="reject"></i></a> -->
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <span class="custom-checkbox">
                                              <input type="checkbox" id="checkbox1" name="options[]" value="1">
                                              <label for="checkbox1"></label>
                                                    </span>
                                    </td>
                                    <td>Football</td>
                                    <td>4-11-2023<br>11:00am</td>
                                    <td>Bir</td>
                                    <td>Ball,flags</td>
                                    <td></td>
                                    <td>Approved</td>
                                    

                                    
                                    
                                    <td>
                                    <button type="button" class="btn btn-success btn-sm" onclick="fireSweetAlertApprove()"><i class="bi bi-check-circle"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" onclick="fireSweetAlertReject()"><i class="bi bi-x-circle"></i></button>
                                    </td>
                                  </tr> 
                                  
                                </tbody>
                                </table>
                        </div>
                    </div>
                </div>  
                </div>

            </div>
            
            
      
    </div>

    

    <script>
        function fireSweetAlertApprove() {
            Swal.fire({
                title: 'Are you sure you want to Approve this booking?',
                
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#5cb85c',
                cancelButtonColor: '#d9534f',
                confirmButtonText: 'Yes, Approve it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                    'Approved'
                    )
                }
                })
        }
    </script>
    <script>
        function fireSweetAlertReject() {
            Swal.fire({
                title: 'Are you sure you want to Reject this booking?',
                
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#5cb85c',
                cancelButtonColor: '#d9534f',
                confirmButtonText: 'Yes, Reject it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                    'Rejected!'
                    )
                }
                })
        }
    </script>
    <script>
        function fireSweetAlertDeleteBooking() {
            Swal.fire({
                title: 'Are you sure you want to Delete booking?',
                
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#5cb85c',
                cancelButtonColor: '#d9534f',
                confirmButtonText: 'Yes, Delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                    'Deleted!'
                    )
                }
                })
        }
    </script>

</x-admin-component>
