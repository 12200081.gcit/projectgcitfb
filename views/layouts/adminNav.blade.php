<!-- main sidebar -->
<div class="fixed-sidebar sidebar-mini">
    <div class="logo"> 
        <a href="index.php"><img src="{{ asset('admin_assets/images/gc1-blue.png') }}" alt="LOGO" style="max-width: 60%;"></a>
        <button class="sidebar-collapse"><i class="bi bi-list"></i></button>
        
    </div>
    
    <!-- sidebar menu 3a84b983 -->
    <div class="menu">
        <div class="custom-scroll">
            <div class="sidebar-menu">
                <ul>
                    <li class="sidebar-title"><span>Menu</span></li>
                    <li class="sidebar-item"><a href="{{ url ('admin/dashboard')}}" class="sidebar-link"  data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard" tabindex="0"><i class="bi bi-grid"></i> <span>Dashboard</span></a></li>
                    <li class="sidebar-item"><a href="{{ url ('admin/facilities')}}" class="sidebar-link"  data-bs-toggle="tooltip" data-bs-placement="right" title="Facilities" tabindex="0"><i class="bi bi-bullseye"></i> <span>Facilities</span></a></li>
                    <li class="sidebar-item"><a href="{{ url ('admin/booking')}}" class="sidebar-link" data-bs-toggle="tooltip" data-bs-placement="right" title="Bookings" tabindex="0"><i class="bi bi-bookmark"></i> <span>Bookings</span></a></li>
                    <li class="sidebar-item"><a href="{{ url ('admin/user')}}" class="sidebar-link" data-bs-toggle="tooltip" data-bs-placement="right" title="Users" tabindex="0"><i class="bi bi-person-circle"></i> <span>Users</span></a></li>
                    <li class="sidebar-item"><a href="{{ url ('admin/setting')}}"class="sidebar-link" data-bs-toggle="tooltip" data-bs-placement="right" title="Setting" tabindex="0"><i class="bi bi-gear"></i> <span>Setting</span></a></li>
                    <li class="sidebar-item">
                      
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <x-dropdown-link :href="route('logout')" class="sidebar-link" data-bs-toggle="tooltip" data-bs-placement="right" tabindex="0"
                                        onclick="event.preventDefault();
                                                    this.closest('form').submit();">
                                                    <i class="bi bi-box-arrow-right"></i>
                                                    <span>Logout</span>
                                </x-dropdown-link>
                            </form>
                      
                    </li>
                    
                </ul>
            </div>
        </div>
    </div>
    <!-- sidebar menu -->
</div>
<!-- main sidebar -->

<script>
   document.addEventListener("DOMContentLoaded", function() {
    // Get all sidebar links
    const sidebarLinks = document.querySelectorAll(".sidebar-link");

    // Add a click event listener to each link
    sidebarLinks.forEach(function(link) {
        link.addEventListener("click", function(e) {
            // Remove the 'active' class from all links
            sidebarLinks.forEach(function(link) {
                link.classList.remove("active");
            });

            // Add the 'active' class to the clicked link
            this.classList.add("active");
        });
    });
});

</script>